package com.pawelbanasik;

public class Contacts {

	private String name;
	private int number;

	public Contacts(String name, int number) {
		this.name = name;
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public int getNumber() {
		return number;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	@Override
	public String toString() {
	
		return "Name: " + name + ", Number: " +  number;
	}

	
	
	
}
