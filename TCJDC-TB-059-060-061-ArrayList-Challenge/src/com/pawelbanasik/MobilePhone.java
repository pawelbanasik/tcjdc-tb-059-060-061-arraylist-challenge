package com.pawelbanasik;

import java.util.ArrayList;
import java.util.List;

public class MobilePhone {

	private List<Contacts> list = new ArrayList<>();

	public void printAll() {
		for (int i = 0; i < list.size(); i++) {
			System.out.println(i+1 + ". Name: " + list.get(i).getName() + ", Number: " + list.get(i).getNumber());
		}

	}

	public void addContact(String name, int number) {
		Contacts newContact = new Contacts(name, number);
		
		
			if (findIndexByName(name) >= 0) {
				System.err.println("Name already exists.");
			} else {
				list.add(newContact);
				System.out.println(name + " added successfully.");

			}
		}
	

	public void modifyContact(String name, Contacts newItem) {
		int index = findIndexByName(name);
		if (index >= 0) {
			list.set(index, newItem);
			System.out.println(name + " modified successfully.");
		} else {
			System.err.println(name + " not found.");
		}
	}

	public void removeContact(String name) {
		if (findIndexByName(name) >= 0) {
			list.remove(findIndexByName(name));
			System.out.println(name + " removed successfully.");
		} else {
			System.err.println(name + " not found.");
		}
	}

	public void findContact(String name) {
		for (Contacts contact : list) {
			if (contact.getName().equals(name)) {
				System.out.println(contact.toString());
			}
		}

	}

	public int findIndexByName(String name) {
		for (Contacts contact : list) {
			if (contact.getName().equals(name)) {
				return list.indexOf(contact);
			}
		}
		return -1;

	}

}
