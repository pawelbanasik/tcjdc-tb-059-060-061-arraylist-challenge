package com.pawelbanasik;

import java.util.Scanner;

public class Main {
	private static MobilePhone mp = new MobilePhone();
	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		boolean quit = false;
		int choice = 0;
		while (!quit) {
			System.out.println("Enter your choice: ");
			choice = sc.nextInt();
			sc.nextLine();

			switch (choice) {
			case 0:
				printInstructions();
				break;
			case 1:
				mp.printAll();
				break;
			case 2:
				addMain();
				break;
			case 3:
				modifyMain();
				break;
			case 4:
				removeMain();
				break;
			case 5:
				findMain();
				break;
			case 6:
				quit = true;
				break;
			default:
				break;
			}

		}

	}

	public static void printInstructions() {
		System.out.println("\nPress ");
		System.out.println("\t 0 - To print choice options.");
		System.out.println("\t 1 - To print the list of contacts.");
		System.out.println("\t 2 - To add an item to the list.");
		System.out.println("\t 3 - To modify an item in the list.");
		System.out.println("\t 4 - To remove an item from the list.");
		System.out.println("\t 5 - To search for an item in the list.");
		System.out.println("\t 6 - To quit the application.");
	}

	public static void addMain() {
		System.out.println("Enter new contact name: ");
		String name = sc.nextLine();
		System.out.println("Enter new contact number: ");
		int number = sc.nextInt();
		mp.addContact(name, number);
	}

	public static void modifyMain() {
		System.out.println("Enter name of contact to modify: ");
		String oldName = sc.nextLine();
		System.out.println("Enter new contact name: ");
		String newName = sc.nextLine();
		System.out.println("Enter new contact number: ");
		int newNumber = sc.nextInt();
		mp.modifyContact(oldName, new Contacts(newName, newNumber));
	}

	public static void removeMain() {
		System.out.println("Enter name to remove: ");
		String name = sc.nextLine();
		mp.removeContact(name);
	}
	
	public static void findMain() {
		System.out.println("Enter name of contact to show: ");
		String name = sc.nextLine();
		mp.findContact(name);
	}

}
